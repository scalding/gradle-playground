package scaldingspoon.gradle

import groovy.transform.InheritConstructors
import groovy.xml.DOMBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.tasks.*
import org.pegdown.*
import org.pegdown.ast.HeaderNode
import org.pegdown.ast.VerbatimNode
import org.pegdown.plugins.PegDownPlugins
import org.pegdown.plugins.ToHtmlSerializerPlugin
import org.python.util.PythonInterpreter
import org.xhtmlrenderer.pdf.ITextRenderer

/**
 * Class to support creating a PDF from markdown
 */
class MarkdownPdfPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.plugins.apply(BasePlugin)
        project.tasks.create(name: 'markdownPdf', overwrite: true, description: 'Creates a PDF from markdown files', type: MarkdownPdfTask)
        project.tasks.create(name: "markdownPdfCodeStyles", overwrite: true, description: "Print a list of available styles for code blocks", type: MarkdownPdfCodeStylesTask)
    }
}

/**
 * Task to list the available styles for the code block
 */
class MarkdownPdfCodeStylesTask extends DefaultTask {
    @TaskAction
    def doit() {
        def interpreter = new PythonInterpreter()
        interpreter.exec('''
from pygments.styles import get_all_styles
styles = list(get_all_styles())
''')
        println "available styles: ${interpreter.get("styles")}"
    }
}

/**
 * Task to generate PDF from markdown
 */
class MarkdownPdfTask extends DefaultTask {
    @OutputFile
    File outputFile

    @OutputFile
    File htmlOutputFile

    @InputFiles
    FileCollection inputFiles

    @InputFile
    @Optional
    File styleFile

    @Optional
    boolean toc = false

    @Optional
    int pdExtensions = Extensions.ALL

    @Optional
    String codeStyle = "colorful"

    @TaskAction
    def doit() {
        def codeStyleCss = {
            PythonInterpreter interpreter = new PythonInterpreter()
            interpreter.set("codeStyle", codeStyle)
            interpreter.exec('''
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name

formatter = HtmlFormatter(style=codeStyle)
style = formatter.get_style_defs('.highlight')
''')
            return interpreter.get("style", String)
        }()

        // get a plugin that supports inline fenced code (to act kinda like github)
        def plugins = PegDownPlugins.builder().withPlugin(PluginParser).build()

        def extendedPegdownProcessor = new ExtendedPegDownProcessor(pdExtensions, plugins)
        def pegdownProcessor = new PegDownProcessor(pdExtensions, plugins)

        def tocString
        if (toc) {
            def tocRawString = extendedPegdownProcessor.markdownToToc(inputFiles.collect {it.text}.join("\n"*2))
            tocString = pegdownProcessor.markdownToHtml(tocRawString)
        }
        def docString = extendedPegdownProcessor.markdownToHtml(inputFiles.collect {it.text}.join("\n"*2), new LinkRenderer(), new PygmentsVerbatimSerializers())
        def domString = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        ${styleFile ? "<link rel='stylesheet' type='text/css' href='${styleFile}' />" : ""}
        <style type="text/css">
            ${codeStyleCss}
        </style>
    </head>
    <body>
        ${if (toc) {"""
        <div id="_toc">
        ${tocString}
        </div>"""}}
        ${docString}
    </body>
</html>"""
        if (htmlOutputFile) {
            htmlOutputFile.withPrintWriter {
                it.println domString
            }
        }
        def doc = DOMBuilder.parse(new StringReader(domString))
        def renderer = new ITextRenderer()
        renderer.setDocument(doc, null)
        renderer.layout()
        renderer.createPDF(outputFile.newOutputStream())
    }
}

/**
 * A cheater class that acts like a map but generates elements ad hoc
 */
class PygmentsVerbatimSerializers {
    @Delegate Map<String, VerbatimSerializer> serializerMap = new HashMap<String, VerbatimSerializer>()

    def get(String key) {
        if (serializerMap.containsKey(key)) {
            return serializerMap.get(key)
        }
        new VerbatimSerializer(){
            @Override
            void serialize(VerbatimNode node, Printer printer) {
                PythonInterpreter interpreter = new PythonInterpreter()
                interpreter.set("code", node.text)
                interpreter.exec("""
from pygments import highlight
from pygments.lexers import ${key.capitalize()}Lexer
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name

formatter = HtmlFormatter(style='colorful')
result = highlight(code, ${key.capitalize()}Lexer(), formatter)
style = formatter.get_style_defs('.highlight')
""")

                printer.print('\n')
                printer.print(interpreter.get("result", String))

                def f = interpreter.get("style")
            }
        }
    }
}

/**
 * Support class that extends PegDownProcessor and utilizes the ExtendedToHtmlSerializer
 */
@InheritConstructors
class ExtendedPegDownProcessor extends PegDownProcessor {
    @Override
    String markdownToHtml(char[] markdownSource, LinkRenderer linkRenderer, Map<String, VerbatimSerializer> verbatimSerializerMap) {
        try {
            def astRoot = parseMarkdown(markdownSource)
            return new ExtendedToHtmlSerializer(linkRenderer, verbatimSerializerMap).toHtml(astRoot)
        } catch (ParsingTimeoutException e) {
            return null
        }
    }

    String markdownToToc(String markdownSource) {
        def writer = new StringWriter()
        def chapterHander = new ChapterHandler()

        writer.append("# Table of Contents\n").append("\n")

        def ast = parseMarkdown(markdownSource.chars)
        ast.children.findAll {it instanceof HeaderNode}.each { HeaderNode header ->
            def chapter = chapterHander.next(header.level)
            writer.append("    "*(header.level-1)).append("1. ").append("[${header.children[0].text}](#_${chapter})").append("\n")
        }


        return writer
    }
}

/**
 * Helper class to generate chapters and subchapters
 */
class ChapterHandler {
    def chapter = [0,0,0,0,0,0]

    def next(int level) {
        if (level > 6) {
            throw new IllegalStateException("can't go deeper than six")
        }
        if (level > 1 && !chapter[level - 2]) {
            throw new IllegalStateException("gotta call a higher level chapter first")
        }
        chapter[level..5] = 0
        chapter[level - 1]++
        return chapter[0..level-1].join(".")
    }
}

/**
 * Support class that supports custom headers based on chapters and code highlighting
 */
class ExtendedToHtmlSerializer extends ToHtmlSerializer {
    private def chapterHandler = new ChapterHandler()

    LinkRenderer linkRenderer
    List<ToHtmlSerializerPlugin> plugins
    Map<String, VerbatimSerializer> verbatimSerializers

    ExtendedToHtmlSerializer(LinkRenderer linkRenderer) {
        super(linkRenderer)
    }

    ExtendedToHtmlSerializer(LinkRenderer linkRenderer, List<ToHtmlSerializerPlugin> plugins) {
        super(linkRenderer, plugins)
    }

    ExtendedToHtmlSerializer(LinkRenderer linkRenderer, Map<String, VerbatimSerializer> verbatimSerializers) {
        this(linkRenderer, verbatimSerializers, [])
    }

    ExtendedToHtmlSerializer(LinkRenderer linkRenderer, Map<String, VerbatimSerializer> verbatimSerializers, List<ToHtmlSerializerPlugin> plugins) {
        super(linkRenderer, [:], plugins)
        this.verbatimSerializers = verbatimSerializers
        if(!this.verbatimSerializers.containsKey(VerbatimSerializer.DEFAULT)) {
            this.verbatimSerializers.put(VerbatimSerializer.DEFAULT, DefaultVerbatimSerializer.INSTANCE);
        }
    }

    public void visit(HeaderNode node) {
        def chap = chapterHandler.next(node.level)
        printer.print("<h${node.level} id='_${chap}'>")
        printer.print("${chap}. ")
        visitChildren(node)
        printer.print("</h${node.level}>")
    }

    @Override
    void visit(VerbatimNode node) {
        def serializer = this.verbatimSerializers.get(node.type ?: VerbatimSerializer.DEFAULT)
        serializer.serialize(node, printer)
    }
}