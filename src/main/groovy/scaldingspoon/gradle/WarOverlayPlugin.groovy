package scaldingspoon.gradle

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.plugins.WarPlugin
import org.gradle.api.tasks.bundling.War

class WarOverlayPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.plugins.apply(WarPlugin)
        project.convention.plugins.warOverlay = new WarOverlayPluginConvention()

        project.tasks.withType(War, new Action<War>() {
            @Override
            void execute(War war) {
                war.doFirst {
                    war.project.configurations.runtime.each {
                        if (it.name.endsWith(".war")) {
                            def fileList = war.project.zipTree(it)
                            def e = project.convention.plugins.warOverlay
                            if (!project.convention.plugins.warOverlay.noWarJars) {
                                war.from fileList
                            } else {
                                war.from fileList.matching { exclude "**/*.jar" }
                            }
                        }
                    }
                }
                war.doFirst {
                    war.classpath = war.classpath.filter {!it.name.endsWith(".war")}
                }
                war.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        })
    }
}

class WarOverlayPluginConvention {
    boolean noWarJars = true

    def warOverlay(Closure c) {
        c.delegate = this
        c()
    }

    def methodMissing(String name, args) {
        this."${name}" = args[0]
    }
}