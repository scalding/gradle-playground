package scaldingspoon.gradle;

import org.parboiled.Rule;
import org.pegdown.Parser;
import org.pegdown.plugins.InlinePluginParser;

public class PluginParser extends Parser implements InlinePluginParser {
    public PluginParser() {
        super(ALL, 2000L, DefaultParseRunnerProvider);
    }

    @Override
    public Rule[] inlinePluginRules() {
        return new Rule[]{FencedCodeBlock()};
    }
}
